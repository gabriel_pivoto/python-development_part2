class Pessoa(object):
    def __init__(self, nome, sexo, idade):
        self.nome = nome
        self.sexo = sexo
        self.idade = idade

class Cidadao(Pessoa):
    def __init__(self, nome, sexo,idade, cpf):
        super().__init__(nome, sexo, idade)
        self.cpf = cpf
    

pessoa = Cidadao("Gabriel", "masc", 123, 123432)    


print(dir(pessoa))