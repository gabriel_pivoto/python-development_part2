class CalculoImposto(object):
    def _init_(self):
        self._taxa = 0

    @property
    def taxa(self):
        return round(self._taxa, 2)

    @taxa.setter
    def taxa(self, val_taxa):
        if val_taxa < 0:
            self._taxa = 0
        elif val_taxa > 100:
            self._taxa = 100
        else:
            self._taxa = val_taxa

class  ControlarDesconto(object):
    def __init__(self):
        self.__preco_produto = 0
        self.__porcentagem_desconto = 1
    
    @property
    def desconto(self):
        self.__preco_produto -= self.__preco_produto*(self.__porcentagem_desconto/100)
        return round(self.__preco_produto, 2)

    @desconto.setter
    def desconto(self, val_porcentagem):
        if val_porcentagem < 1:
            self.__porcentagem_desconto = 1
        elif val_porcentagem > 35:
            self.__porcentagem_desconto = 1
        else:
            self.__porcentagem_desconto = val_porcentagem

    @property
    def preco_produto(self):
        return self.__preco_produto
    
    @preco_produto.setter
    def preco_produto(self, preco):
        if preco >= 0:
            self.__preco_produto = preco
        else:
            print("Insira um valor de preço positivo")
        


    