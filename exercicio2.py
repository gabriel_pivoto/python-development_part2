class Banco(object):
    def __init__(self, numeroConta, nome):
        self.numeroConta = numeroConta
        self.nome = nome
        self.saldo = 0

    def alterar_nome(self, nome):
        self.nome = nome

        return self.nome
    
    def depositar(self, deposito):
        self.saldo += deposito
        return self.saldo
    
    def sacar(self, sacar):
        if self.saldo - sacar >= 0:
            self.saldo -= sacar
            return self.saldo
        else:
            return "Saldo insuficiente"

pessoa = Banco(12, "gabriel")
while True:
    choice = int(input("1 - alterar nome\n2 - depositar\n3 - sacar\n4 - Sair\nDigite a opção: "))

    match choice:
        case 1:
            novo_nome = str(input("Insira seu novo nome: "))
            print(pessoa.alterar_nome(novo_nome))
            
        case 2: 
            deposito = int(input("Digite o valor que deseja depositar: "))
            print(pessoa.depositar(deposito))
            
        case 3:
            sacar = int(input("Digite o valor que deseja sacar: "))
            print(pessoa.sacar(sacar))
            
        case 4:
            print("Tchau :(")
            break
        case _:
            print("Valor inserido inválido")
    
        
    